function PullFrame_Pull(time)
   Print(format("Pull toggled (%d sec)", time))
   Pull.chan = PullChan()
   Pull.timer = time
   Pull.running = not Pull.running
   if not Pull.running then
      PullMsg("PULL ABORTED!!!")
      PullFrame_OnLoad()
   end
end

function PullFrame_OnLoad()
   Pull = {};
   Pull.running = false;
   Pull.lastupdate = 1;
   Pull.timer = 6;
   Pull.chan = "PARTY"
end

function PullFrame_OnUpdate()
   if Pull.running then
      Pull.lastupdate = Pull.lastupdate + arg1
      if (Pull.lastupdate > 1) then
	 PullFrame_Fire()
	 Pull.lastupdate = Pull.lastupdate - 1
      end
   end
end

function PullFrame_Fire()
   if(Pull.timer >= 60) then
      if(modulo(Pull.timer, 30) == 0) then
	 PullFrame_Msg(Pull.timer)
      end
   elseif(Pull.timer < 60 and Pull.timer >= 30) then
      if(modulo(Pull.timer, 10) == 0) then
	 PullFrame_Msg(Pull.timer)
      end
   elseif(Pull.timer < 30 and Pull.timer >= 10) then
      if(modulo(Pull.timer, 5) == 0) then
	 PullFrame_Msg(Pull.timer)
      end
   elseif(Pull.timer < 10 and Pull.timer > 0) then
      PullFrame_Msg(Pull.timer)
   elseif(Pull.timer == 0) then
      PullMsg("=== PULL NOW! ===")
      PullFrame_OnLoad()
   else
      Print("This should never happen")
      PullMsg("XXX PullTimer panicked - abort XXX")
   end
   Pull.timer = Pull.timer - 1
end

function PullFrame_Msg(time)
   PullMsg(format("Pull in %d sec", time))
end
--------------------------------------------------------------------------------
SLASH_PULL1= '/pull'
function SlashCmdList.PULL(time)
   if time == "" then
      time = 6
   elseif(type(time)=='string') then
      time = tonumber(time)
   end
   
   PullFrame_Pull(time)
end

--------------------------------------------------------------------------------
function Print(msg)
   DEFAULT_CHAT_FRAME:AddMessage("[PullTimer] "..msg)
end

function PullMsg(msg)
   SendChatMessage(msg, Pull.chan)
end

function PullChan(msg)
   chan = "SAY"
   if GetNumPartyMembers() > 0 then chan = "PARTY" end
   if UnitInRaid("player") then chan = "RAID" end
   if IsRaidOfficer() and UnitInRaid("player") then chan = "RAID_WARNING" end
   if IsRaidLeader() and UnitInRaid("player") then chan = "RAID_WARNING" end
   return chan
end

function modulo(a,b)
   return a - math.floor(a/b)*b
end

