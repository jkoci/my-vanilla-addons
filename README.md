# Installation

1. Clone or download the repository.
1. Copy each folder into `(your wow folder)/Interface/Addons`.
1. Allow selected addons in interface.

# License
Licensed under WTFPL, see [LICENSE](LICENSE).

# Manual
## PullTimer

* `/pull [time=6 sec]`
* While running, `/pull` aborts.